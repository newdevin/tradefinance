﻿CREATE TABLE [dbo].[Lender]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [CounterpartyId] INT NOT NULL REFERENCES dbo.Counterparty(Id)
)
