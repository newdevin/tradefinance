﻿CREATE TABLE [dbo].[Counterparty]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [Name] NVARCHAR(50) NULL UNIQUE, 
    [CounterpartyType] INT NOT NULL FOREIGN KEY REFERENCES dbo.CounterpartyType(Id)
)
