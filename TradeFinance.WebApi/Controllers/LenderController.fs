﻿namespace TradeFinance.Controllers
open Microsoft.AspNetCore.Mvc
open TradeFinance.Domain
open TradeFinance
open TradeFinance.Service
open System.Runtime.InteropServices.ComTypes
open System.Collections

[<Route("api/[controller]")>]
[<ApiController>]
type LenderController () =
    inherit ControllerBase()

    [<HttpGet>]
    member this.GetLenders() = 
        let lenders = Service.getAllLenders (Database.getLenders)
        ActionResult<Lender list>(lenders)



