﻿// Learn more about F# at http://fsharp.org

open System
open TradeFinance

[<EntryPoint>]
let main argv =
    let counterparty = Database.getCounterparty "Barclays"
    match counterparty with
    | Some c -> printfn "%s" c.Name
    | None -> printfn "not found"
    
    0 // return an integer exit code
