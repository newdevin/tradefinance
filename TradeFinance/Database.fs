﻿namespace TradeFinance

module Database = 
    open Domain
    open FSharp.Data.Sql
            
    let [<Literal>] connectionString = "Database=TradeFinance;Server=.;INTEGRATED SECURITY=TRUE";

    type private db = SqlDataProvider<Common.DatabaseProviderTypes.MSSQLSERVER, connectionString>
    let private ctx = db.GetDataContext()
    
    let getLenders ()  = 
        query {
            for l in ctx.Dbo.Lender do
                join c in ctx.Dbo.Counterparty on (l.CounterpartyId = c.Id)
                join ct in ctx.Dbo.CounterpartyType on (c.CounterpartyTypeId = ct.Id )
                select {Name = c.Name ; Counterparty = { Name = c.Name ; CounterpartyType = {Name = ct.Name}} }
        }
        |> Seq.toList

    let getCounterparty (name:string) = 
        let cp = 
            query {
                for c in ctx.Dbo.Counterparty do
                where  (c.Name = name)
                select c
            }
        match Seq.isEmpty cp with
        | true -> None
        | false -> Some (Seq.head cp)

    let lenderExist (name:string) = 
        let l =
            query {
                for c in ctx.Dbo.Counterparty do
                join l in ctx.Dbo.Lender  on (c.Id = l.CounterpartyId)
                where (c.Name = name)
                select l
        }
        not (Seq.isEmpty l)
      
       
    let addLender (lender: Lender) : Option<Lender> =
        if lenderExist lender.Name then None else
            getCounterparty (lender.Name)
            |>  Option.map  (fun cp -> cp.Id ) 
            |>  Option.map  (fun cpId -> 
                                let newLender = ctx.Dbo.Lender.Create(cpId)
                                ctx.SubmitUpdates()
                                lender
                                )
        
        
        
        


        






        
        

        