﻿
namespace TradeFinance


module Service =
    open Domain
        
    type LenderAdder = Lender -> Option<Lender>

    type GetLenders = unit -> Lender list

    let getAllLenders (getLenders:GetLenders) = 
        getLenders()

    let addLender (lenderAdder:LenderAdder) (lender:Lender) = 
        match lender.Counterparty.CounterpartyType.Name with
        | "Bank" -> Some (lenderAdder lender) 
        | _ -> None



        

